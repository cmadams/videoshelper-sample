module VideosHelper
  
  require "net/http"
  require "uri"

  # Check if URL follows a valid format of a youtube video url, if it is return the ID fragment.
  # Regex from http://stackoverflow.com/questions/2964678/jquery-youtube-url-validation-with-regex?rq=1
  def youtubeVidId(url)
    p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/
    return (url.match(p)) ? $1 : false
  end

  # Check if URL follows a valid format of a vimeo video url, if it is return the ID fragment.
  # Regex from http://stackoverflow.com/questions/2916544/parsing-a-vimeo-id-using-javascript
  def vimeoVidId(url)
    p = /^.*(vimeo\.com\/)((channels\/[A-z]+\/)|(groups\/[A-z]+\/videos\/))?([0-9]+)$/
    return (url.match(p)) ? $5 : false
  end

  # Ask youtube if a video ID points at a real, valid youtube video.
  def valid_youtube_id(youtube_id)
    uri = URI.parse("http://gdata.youtube.com/feeds/api/videos/#{youtube_id}")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    response.code == 200 ? true : false
  end

  # Ask vimeo if a video ID points at a real, valid vimeo video.
  def valid_vimeo_id(vimeo_id)
    uri = URI.parse("http://vimeo.com/api/v2/video/#{vimeo_id}.json")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    response.code == 200 ? true : false
  end

end
